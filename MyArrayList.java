public class MyArrayList<T> implements MyArray<T>{

    private T[] values;

    public MyArrayList(){
        values = (T[]) new Object[0];
    }

    @Override
    public boolean add(T e) {
        try {
            T[] temp = values;
            values = (T[]) new Object[temp.length + 1];
            System.arraycopy(temp, 0, values, 0, temp.length);
            values[values.length - 1] = e;
            return true;
        } catch (ClassCastException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean remove(int index) {
        try {
            T[] temp = values;
            values = (T[]) new Object[temp.length - 1];
            System.arraycopy(temp, 0, values, 0, index);
            System.arraycopy(temp, index + 1, values, index, temp.length - index - 1);
            return true;
        }
        catch (ClassCastException ex) {
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean remove(Object o) {
        int index = -1;
        for(int i = 0; i < values.length; i++) {
            if (values[i].equals(o))
                index = i;
        }
        if (index != -1) {
            remove(index);
            return true;
        }
        return false;
    }

    @Override
    public T get(int index) {
        return values[index];
    }

    @Override
    public int size() {
        return values.length;
    }

    @Override
    public boolean isEmpty() {
        return values.length == 0;
    }

    @Override
    public boolean clear() {
        try {
            values = (T[]) new Object[0];
            return true;
        } catch (ClassCastException ex){
            ex.printStackTrace();
        }
        return false;
    }

    @Override
    public boolean set(int index, T e) {
        if (index >= values.length) return false;
        values[index] = e;
        return true;
    }

    @Override
    public boolean contains(Object o) {
        for (T value : values) {
            if (value.equals(o))
                return true;
        }
        return false;
    }
}
