public interface MyArray<T>{
    boolean add(T e);
    boolean remove(int index);
    boolean remove(Object o);
    T get(int index);
    int size();
    boolean isEmpty();
    boolean clear();
    boolean set(int index, T e);
    boolean contains(Object o);
}
