public class Main {
    public static void main(String[] args) {
        MyArrayList<String> words = new MyArrayList<>();
        words.add("Class");
        words.add("Join");
        words.add("Main");
        words.add("Index");

        System.out.println(words.contains("Main"));
        words.remove(2);
        System.out.println(words.contains("Main"));

        System.out.println("----------");
        System.out.println(words.contains("Class"));
        words.remove("Class");
        System.out.println(words.contains("Class"));

        System.out.println("----------");
        System.out.println(words.size());
        System.out.println(words.isEmpty());
        words.clear();
        System.out.println(words.size());
        System.out.println(words.isEmpty());

    }
}
